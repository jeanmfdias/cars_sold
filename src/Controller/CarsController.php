<?php

namespace App\Controller;

use App\Entity\Car;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CarsController extends AbstractController
{
    public function __construct(private CarRepository $carRepository)
    {
    }

    #[Route('/cars', name: 'cars_index', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $cars = $this->carRepository->findAll();

        return $this->render('cars/index.html.twig', compact('cars'));
    }

    #[Route('/cars/create', name: 'cars_create', methods: ['GET'])]
    public function create(): Response
    {
        return $this->render('cars/form.html.twig');
    }

    #[Route('/cars', name: 'cars_store', methods: ['POST'])]
    public function store(Request $request): Response
    {
        $name = $request->request->get('name');

        $car = new Car($name);
        $this->carRepository->add($car, true);

        $this->addFlash('success', 'Car was saved');

        return new RedirectResponse('/cars');
    }

    #[Route('/cars/delete/{id}', name: 'cars_delete', methods: ['DELETE'], requirements: ['id' => '[0-9]+'])]
    public function delete(int $id, Request $request): Response
    {
        $this->carRepository->removeById($id);

        $this->addFlash('success', 'Car was deleted');

        return new RedirectResponse('/cars');
    }

    #[Route('/cars/edit/{car}', name: 'cars_edit', methods: ['GET'], requirements: ['car' => '[0-9]+'])]
    public function edit(Car $car): Response
    {
        return $this->render('cars/form.html.twig', compact('car'));
    }

    #[Route('/cars/edit/{car}', name: 'cars_update', methods: ['PATCH'], requirements: ['car' => '[0-9]+'])]
    public function update(Car $car, Request $request): Response
    {
        $name = $request->request->get('name');

        $this->carRepository->updateName($car, $name);

        $this->addFlash('success', 'Car was updated');

        return new RedirectResponse('/cars');
    }
}
